import {GraphQLSchema, Kind} from 'graphql';
import {INodes, PluginFunction} from '@/types';
import enumsHandler from './handlers/enumsHandler';
import {nodesHandler, nodesHandlerFormData} from './handlers/nodesHandler';
import {scalarsHandler} from './handlers/scalarsHandler';
import schemaVisitor from './handlers/schemaVisitor';
import {documentNodesHandler, documentNodesHandlerFormData} from "./handlers/documentNodesHandler";
import {defaultInputHandler, defaultInputHandlerFormData} from "./handlers/defaultInputHandler";
export type {Config} from '@/types/codegen'

export const plugin: PluginFunction = (schema: GraphQLSchema, documents, config) => {
    const {enums, nodes, scalars, types, objectTypes} = schemaVisitor(schema, config)
    const documentNodes: INodes[] = []

    documents.forEach(document => { // @ts-ignore
        const definition = document.document?.definitions[0] || null

        if (!definition || (definition.kind !== Kind.OPERATION_DEFINITION) || (definition?.variableDefinitions?.length === 0)) {
            return
        }

        // @ts-ignore
        documentNodes.push({ // @ts-ignore
            name: definition.name?.value, // @ts-ignore
            fields: definition.variableDefinitions || [],
            operation: definition.operation,
        })
    })

    const parsedEnums = enumsHandler(enums, types, config)
    const parsedNodes = nodesHandler(nodes, config, types)
    const parsedFormDataNodes = config.exportFormDataSchema ? nodesHandlerFormData(nodes, config, types) : ''
    const parsedScalars = scalarsHandler(scalars, types, config)
    const parsedDocumentNodes = documentNodesHandler(documentNodes, config, types)
    const parsedDocumentNodesFormData = config.exportFormDataSchema ? documentNodesHandlerFormData(documentNodes, config, types) : ''
    const parsedDefaultInputNodes = defaultInputHandler(objectTypes, config, types)
    const parsedDefaultInputNodesFormData = config.exportFormDataSchema ? defaultInputHandlerFormData(objectTypes, config, types) : ''

    const imports = [
        `import { z } from 'zod'`,
        `import * as Types from '${config.typesImportPath}'`,
    ]

    if (config.zodSchemasMap) {
        imports.push(`import { zfd } from "zod-form-data"`)
    }

    return [
        imports.join('\n'),
        `
type Properties<T> = Required<{
  [K in keyof T]: z.ZodType<T[K], any, T[K]>;
}>`,
        parsedScalars,
        parsedEnums,
        parsedNodes,
        parsedDocumentNodes,
        parsedDefaultInputNodes,
        parsedDocumentNodesFormData,
        parsedFormDataNodes,
        parsedDefaultInputNodesFormData,
    ].join('\n\n')
};
