import {PluginFunction as CodegenPluginFunction} from '@graphql-codegen/plugin-helpers';

export type Config = {
    defaultRequiredMessage?: string
    onlyWithValidation?: Boolean
    zodSchemasMap: { [key: string]: string }
    zodTypesMap: { [key: string]: string }
    typesImportPath: string
    exportFormDataSchema?: boolean
}

export type PluginFunction = {} & CodegenPluginFunction<Config>