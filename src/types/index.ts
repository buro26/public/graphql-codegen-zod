// TYPES
import {InputValueDefinitionNode, OperationTypeNode} from 'graphql';
import {NamedTypeNode, TypeNode} from 'graphql';

export interface INodes {
    name: string
    fields: InputValueDefinitionNode[]
    operation?: OperationTypeNode
}

export interface IObjectTypes {
    name: string
    fields: InputValueDefinitionNode[]
    operation: OperationTypeNode
}

export interface IEnums {
    [key: string]: string[]
}

export interface IScalars {
    [key: string]: string
}

export interface ITypes {
    [key: string]: string
}

export interface IHandled {
    nodes: INodes[]
    enums: IEnums
}

export const isNamed = (type: NamedTypeNode | TypeNode): type is NamedTypeNode => {
    return (type as NamedTypeNode).name !== undefined;
}

export * from './codegen';