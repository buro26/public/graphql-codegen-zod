import {isBoolean, isNumber, isRef, isString} from '@/utils';

export const fieldNamedTypeHandler = (type: string) => {
    let result = 'z.';

    if (isRef(type)) {
        result = `z.lazy(() => ${type}Schema())`
    } else if (isBoolean(type)) {
        result = result + 'coerce.boolean()'
    } else if (isString(type)) {
        result = result + 'string()'
    } else if (isNumber(type)) {
        result = result + 'coerce.number()'
    } else {
        // Assume it's a defined schema!
        result = `z.lazy(() => ${type}Schema())`
    }

    return result
}


export const fieldNamedTypeHandlerFormData = (fieldType: string, currentTypeValue: string, isOptional: boolean) => {
    if (isBoolean(fieldType)) {
        return `zfd.checkbox()${isOptional ? '.optional()' : ''}`
    }

    if (isString(fieldType)) {
        return `zfd.text(${currentTypeValue})`
    }

    if (isNumber(fieldType)) {
        return `zfd.numeric(${currentTypeValue})`
    }

    return currentTypeValue
}