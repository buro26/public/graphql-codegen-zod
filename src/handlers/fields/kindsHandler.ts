import {NamedTypeNode, TypeNode} from 'graphql';
import {isNamed} from '@/types';
import {isArray, isRequired, isType} from '@/utils';
import {fieldNamedTypeHandler, fieldNamedTypeHandlerFormData} from './namedTypesHandlers';

type FieldKindHandler = (args: {
    fieldName: string
    type: NamedTypeNode | TypeNode
    extra: string
    isOptional: boolean
    wrapFormDataType: boolean
}) => string

const fieldKindHandler: FieldKindHandler = (
    {
        fieldName,
        type,
        extra = '',
        isOptional = true,
        wrapFormDataType = false,
    }
) => {
    let result = ''
    if (fieldName === 'arrayRequired') {
        console.log({fieldName, type, extra, isOptional})
    }

    if (isRequired(type.kind)) {
        result = `${fieldKindHandler({
            // @ts-expect-error
            type: type.type,
            fieldName,
            extra,
            isOptional: false,
            wrapFormDataType,
        })}`
    }

    if (isArray(type.kind)) {
        result = `z.array(${fieldKindHandler({
            // @ts-expect-error
            type: type.type,
            fieldName,
            extra,
            isOptional: true,
            wrapFormDataType,
        })})`

        if (isOptional) {
            result = `${result}.nullish().optional()`
        }

        if (wrapFormDataType) {
            result = `zfd.repeatable(${result})`
        }
    }

    if (isType(type.kind) && isNamed(type)) {
        result = fieldNamedTypeHandler(type.name.value)
        result = `${result}${extra}`

        if (isOptional) {
            result = `${result}.nullish().optional()`
        }

        if (wrapFormDataType) {
            result = fieldNamedTypeHandlerFormData(type.name.value, result, isOptional)
        }
    }

    return result
}

export default fieldKindHandler
