import { InputValueDefinitionNode } from 'graphql';
import fieldHandler from './fieldHandlers';
import {Config} from "@/types/codegen";

const fieldsHandler = (fields: InputValueDefinitionNode[], config: Config, wrapFormDataType: boolean) => {
  return fields.map((field) => fieldHandler(field, config, wrapFormDataType)).join(',\n        ')
}

export default fieldsHandler
