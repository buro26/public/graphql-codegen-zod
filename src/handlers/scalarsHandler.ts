import {Config, IScalars, ITypes} from '@/types';

export const scalarsHandler = (scalars: IScalars, types: ITypes, config: Config): string => {
    return Object.keys(scalars)
        .map((key) => {
            const schemaName = `${key}Schema`
            let typeDef = ''

            if (types[key]) {
                typeDef += `z.ZodSchema<${types[key]}>`
            } else {
                typeDef += `z.ZodSchema<any>`
            }

            return `
export function ${schemaName}(): ${typeDef} {
    return ${scalars[key]}
}`
        })
        .join('\n')
}


