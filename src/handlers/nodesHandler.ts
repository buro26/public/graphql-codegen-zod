import {Config, INodes, ITypes} from '@/types';
import fieldsHandler from './fields';

const convertTypeName = (name: string) => {
    if (name === 'IDFilterInput') {
        return 'IdFilterInput'
    }

    if (name === 'JSONFilterInput') {
        return 'JsonFilterInput'
    }

    return name
}

const nameToUpperCase = (name: string) => {
    return name?.charAt(0)?.toUpperCase() + name?.slice(1)
}

export const nodesHandler = (nodes: INodes[], config: Config, types: ITypes) => {
    return nodes
        .map(({name, fields}) => {
            const fieldsZod = fieldsHandler(fields, config, false)
            const schemaName = `${name}Schema`
            let typeDef = ''

            if (types[name]) {
                typeDef += `z.ZodObject<${types[nameToUpperCase(convertTypeName(name))]}>`
            } else {
                typeDef += `z.ZodObject<Properties<Types.${nameToUpperCase(convertTypeName(name))}>>`
            }

            return `
export function ${schemaName}(): ${typeDef} {
    return z.object({
        ${fieldsZod}
    }) as ${typeDef}
}`
        })
        .join('\n\n\n')
}

export const nodesHandlerFormData = (nodes: INodes[], config: Config, types: ITypes) => {
    if (!config.exportFormDataSchema) return ''

    return nodes
        .map(({name, fields}) => {
            const fieldsZod = fieldsHandler(fields, config, true)
            const schemaName = `${nameToUpperCase(name)}FormDataSchema`

            return `
export function experimental_${schemaName}() {
    return zfd.formData({
        ${fieldsZod}
    })
}`
        })
        .join('\n\n\n')
}
