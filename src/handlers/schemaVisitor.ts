import {printSchemaWithDirectives} from '@graphql-tools/utils';
import {GraphQLSchema, InputValueDefinitionNode, parse, visit} from 'graphql';
import {Config, IEnums, INodes, IObjectTypes, IScalars, ITypes} from '@/types';
import {DIRECTIVE_NAME} from '@/utils';

const schemaVisitor = (schema: GraphQLSchema, config: Config) => {
    const printedSchema = printSchemaWithDirectives(schema)

    const astNode = parse(printedSchema)

    const nodes: INodes[] = []
    const enums: IEnums = {}
    const scalars: IScalars = {}
    const types: ITypes = {}
    const objectTypes: IObjectTypes[] = []

    visit(astNode, {
        InputObjectTypeDefinition: {
            leave: (node) => {
                let hasValidation = Boolean(!config.onlyWithValidation)

                if (!hasValidation) {
                    node.fields?.forEach((field: InputValueDefinitionNode) => {
                        const validation = field.directives?.find(
                            (directive) => directive.name.value === DIRECTIVE_NAME,
                        );
                        if (validation) {
                            hasValidation = true
                        }
                    })
                }
                if (hasValidation) nodes.push({name: node.name.value, fields: [...node.fields!]})

                if (config.zodTypesMap?.hasOwnProperty(node.name.value)) {
                    types[node.name.value] = config.zodTypesMap[node.name.value]
                }
            },
        },
        EnumTypeDefinition: {
            leave: (node) => {
                if (node.values) {
                    enums[node.name.value] = node.values?.map((e: any) => e.name.value)
                }
            },
        },
        ScalarTypeDefinition: {
            leave: (node) => {
                if (config.zodSchemasMap?.hasOwnProperty(node.name.value)) {
                    scalars[node.name.value] = config.zodSchemasMap[node.name.value]
                } else {
                    scalars[node.name.value] = 'z.any()'
                }

                if (config.zodTypesMap[node.name.value]) {
                    types[node.name.value] = config.zodTypesMap[node.name.value]
                }
            }
        },
        ObjectTypeDefinition: {
            leave: (node) => {
                if (node.name.value !== 'Query' && node.name.value !== 'Mutation') {
                    return
                }

                node.fields?.forEach((field) => {
                    objectTypes.push({
                        name: field.name.value, // @ts-ignore
                        fields: field.arguments || [], // @ts-ignore
                        operation: node.name.value,
                    })
                })
            }
        }
    })

    return {nodes, enums, scalars, types, objectTypes}
}

export default schemaVisitor
