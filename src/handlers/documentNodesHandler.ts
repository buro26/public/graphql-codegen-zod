import {Config, INodes, ITypes} from '@/types';
import fieldsHandler from './fields';

const convertTypeName = (name: string) => {
    if (name === 'IDFilterInput') {
        return 'IdFilterInput'
    }

    if (name === 'JSONFilterInput') {
        return 'JsonFilterInput'
    }

    return name
}

const nameToUpperCase = (name: string) => {
    return name?.charAt(0)?.toUpperCase() + name?.slice(1)
}

export const documentNodesHandler = (nodes: INodes[], config: Config, types: ITypes) => {
    return nodes // @ts-ignore
        .map(({name, fields, operation}) => {
            const fieldsZod = fieldsHandler(fields, config, false)
            const schemaName = `${nameToUpperCase(name)}SchemaGenerated`
            let typeDef = ''

            if (types[name]) { // @ts-ignore
                typeDef += `z.ZodObject<Properties<Types.${nameToUpperCase(types[convertTypeName(name)])}${nameToUpperCase(operation)}Variables>>`
            } else { // @ts-ignore
                typeDef += `z.ZodObject<Properties<Types.${nameToUpperCase(convertTypeName(name))}${nameToUpperCase(operation)}Variables>>`
            }

            return `
export function ${schemaName}(): ${typeDef} {
    return z.object({
        ${fieldsZod}
    }) as ${typeDef}
}`
        })
        .join('\n\n\n')
}


export const documentNodesHandlerFormData = (nodes: INodes[], config: Config, types: ITypes) => {
    return nodes // @ts-ignore
        .map(({name, fields, operation}) => {
            const fieldsZod = fieldsHandler(fields, config, true)
            const schemaName = `${nameToUpperCase(name)}FormDataSchemaGenerated`

            return `
export function experimental_${schemaName}() {
    return zfd.formData({
        ${fieldsZod}
    })
}`
        })
        .join('\n\n\n')
}

