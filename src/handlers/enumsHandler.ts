import {Config, IEnums, ITypes} from '@/types';

function toPascalCase(input: string): string {
    if (!input.includes('_')) {
        return input
    }

    return input
        .split('_')
        .map(part => part.charAt(0).toUpperCase() + part.slice(1).toLowerCase())
        .join('_')
}

function convertTypeName(name: string) {
    if (name === 'Navigationrendertype') {
        return 'NavigationRenderType'
    }

    if (name === 'Navigationtype') {
        return 'NavigationType'
    }

    if (name === 'Publicationstate') {
        return 'PublicationState'
    }

    return name
}

const enumsHandler = (enums: IEnums, types: ITypes, config: Config): string => {
    return Object.keys(enums)
        .map((key) => {
            let schemaName = `${key}Schema`
            let typeDef = ''

            if (types[key]) {
                typeDef += `z.ZodSchema<${types[key]}>`;
            } else {
                typeDef += `z.ZodSchema<Types.${convertTypeName(toPascalCase(key))}>`;
            }

            return `
export function ${schemaName}(): ${typeDef} {
    return z.nativeEnum(Types.${convertTypeName(toPascalCase(key))})
}`
        })
        .join('\n')
};

export default enumsHandler
