import {Config, IObjectTypes, ITypes} from '@/types';
import fieldsHandler from './fields';

const convertTypeName = (name: string) => {
    if (name === 'IDFilterInput') {
        return 'IdFilterInput'
    }

    if (name === 'JSONFilterInput') {
        return 'JsonFilterInput'
    }

    return name
}

const nameToUpperCase = (name: string) => {
    return name?.charAt(0)?.toUpperCase() + name?.slice(1)
}

export const defaultInputHandler = (nodes: IObjectTypes[], config: Config, types: ITypes) => {
    return nodes
        .map(({name, fields, operation}) => {
            const fieldsZod = fieldsHandler(fields, config, false)
            const schemaName = `${nameToUpperCase(name)}${nameToUpperCase(operation)}InputSchema`
            let typeDef = ''

            if (types[name]) { // @ts-ignore
                typeDef += `z.ZodObject<Properties<Types.${nameToUpperCase(operation)}${nameToUpperCase(types[convertTypeName(name)])}}Args>>`
            } else { // @ts-ignore
                typeDef += `z.ZodObject<Properties<Types.${nameToUpperCase(operation)}${nameToUpperCase(convertTypeName(name))}Args>>`
            }

            if (fields.length < 1) {
                typeDef= 'z.ZodObject<{}>'
            }

            return `
export function ${schemaName}(): ${typeDef} {
    return z.object({
        ${fieldsZod}
    }) as ${typeDef}
}`
        })
        .join('\n\n\n')
}


export const defaultInputHandlerFormData = (nodes: IObjectTypes[], config: Config, types: ITypes) => {
    if (!config.exportFormDataSchema) return ''

    return nodes
        .map(({name, fields, operation}) => {
            const fieldsZod = fieldsHandler(fields, config, true)
            const schemaName = `${nameToUpperCase(name)}${nameToUpperCase(operation)}InputFormDataSchema`

            return `
export function experimental_${schemaName}() {
    return zfd.formData({
        ${fieldsZod}
    })
}`
        })
        .join('\n\n\n')
}

