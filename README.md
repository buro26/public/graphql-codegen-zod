# GraphQL Codegen Zod (Form Data)

Generate zod schemas for your GraphQL API using GraphQL Codegen.

## Getting started

```bash
npm i --save-dev buro26-graphql-codegen-zod @graphql-codegen/cli @graphql-codegen/schema-ast @graphql-codegen/typescript @graphql-codegen/typescript-operations @graphql-codegen/typed-document-node
```

## Usage

Create a codegen.ts file in the root of your project with the following content:

```typescript
import type {CodegenConfig} from '@graphql-codegen/cli'

const config: CodegenConfig = {
    schema: 'http://localhost:1337/graphql',
    documents: 'src/lib/strapi/queries/**/!(*.generated).{ts,tsx}',
    debug: true,
    overwrite: true,
    verbose: true,
    generates: {
        // optional: generate schema types
        './schema.graphql': {
            plugins: ['schema-ast']
        },
        'src/lib/my-api/generated/types.ts': {
            plugins: ['typescript', 'typescript-operations', 'typed-document-node'],
            config: {
                withHooks: false,
                maybeValue: 'T | null',
                avoidOptionals: false
            }
        },
        'src/lib/my-api/generated/client-factory.ts': {
            plugins: ['buro26-graphql-codegen-client'],
            config: {
                logger: true,
                typesImportPath: '@/lib/strapi/generated/types',
                schemaImportPath: '@/lib/strapi/generated/schema'
            }
        },
        'src/lib/my-api/generated/schema.ts': {
            plugins: ['buro26-graphql-codegen-zod'],
            config: {
                onlyWithValidation: false,
                lazy: true,
                zodTypesMap: {
                    DateTime: 'string',
                    JSON: 'string',
                    ID: 'string',
                    Int: 'number',
                    Float: 'number',
                    Long: 'number',
                    String: 'string',
                    Boolean: 'boolean',
                    I18NLocaleCode: 'string'
                },
                zodSchemasMap: {
                    DateTime: 'z.string()',
                    JSON: 'z.any()',
                    Long: 'z.coerce.number()',
                    I18NLocaleCode: 'z.string()'
                }
            }
        }
    }
}
export default config
```

Add a script to your package.json to run the codegen:

```json
{
  "scripts": {
    "codegen": "graphql-codegen"
  }
}
```

Run the codegen script:

```bash
npm run codegen
```

## Zod Form Data (experimental)

By enabling the `exportFormDataSchema` option, you can generate schemas based
on [zod-form-data](https://github.com/airjp73/remix-validated-form).

## Test and Deploy

### Running tests

To run tests, run the following command:

```bash
bun test
```

## Contributing

Wish to contribute to this project? Pull the project from the repository and create a merge request.

## Authors and acknowledgment

Buro26 - https://buro26.digital  
Special thanks to all contributors and the open-source community for their support and contributions.

## License

This project is licensed under the MIT License - see the LICENSE file for details.

## Project status

The project is currently in active development. We are continuously working on adding new features and improving the
existing ones. Check the issues section for the latest updates and planned features.

Feel free to reach out if you have any questions or suggestions!